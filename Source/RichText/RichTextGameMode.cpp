// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "RichTextGameMode.h"
#include "RichTextCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARichTextGameMode::ARichTextGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
