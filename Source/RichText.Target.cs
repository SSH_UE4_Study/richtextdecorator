// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class RichTextTarget : TargetRules
{
	public RichTextTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.AddRange(new string[]
        {
            "RichText",
            "SSH_HelperModule"
        });
    }
}
